from markdown_it import MarkdownIt
import subprocess
from pathlib import Path
import shutil

from mdit_py_plugins.front_matter import front_matter_plugin
from mdit_py_plugins.footnote import footnote_plugin
from mdutils import MdUtils


def create_or_reset_if_exist(path):
    if path.exists():
        shutil.rmtree(path, ignore_errors=False, onerror=None)
        path.mkdir(parents=True, exist_ok=True)
        print("Reset and Create ", path)
    else:
        path.mkdir(parents=True, exist_ok=True)
        print("Create ", path)

def run_decktape(path):
    # to use decktape locally, uncomment
    #subprocess.run(["decktape", "reveal",  path / "index.html", "index.pdf", "--screenshots", "true", "--chrome-arg=--allow-file-access-from-files"])
    subprocess.run(["decktape", "reveal",  path / "index.html", "index.pdf", "--screenshots", "true","--chrome-path","chromium","--chrome-arg=--no-sandbox", "--chrome-arg=--allow-file-access-from-files"])

def get_img_slides(path):
    pngFiles = list(path.glob('**/*.png'))
    number_slides = map (lambda x: int(x.stem.split("_")[1]), pngFiles)
    dictPngFiles = dict(zip(number_slides,pngFiles))
    return dictPngFiles

def get_md_comments(path):
    mdFiles = list(path.glob('**/*.md'))
    number_md = map(lambda x: int(x.stem), mdFiles)
    dictMdFiles = dict(zip(number_md, mdFiles))
    return dictMdFiles

def generate_globalMdFile(mdFile, path, dict_img, dict_comment):

    sorted_img = dict(sorted(dict_img.items(), key=lambda item: item[0]))

    for i, (k, v) in enumerate(sorted_img.items()):
        mdFile.new_header(level=2, title='slide {number}'.format(number=k) )
        img = mdFile.new_inline_image("bla", str(Path.relative_to(v,path)))
        mdFile.write(img)

        comment_file = dict_comment.get(k)
        if comment_file:
            comment_text = comment_file.read_text()
            mdFile.new_paragraph(comment_text)

    return mdFile

def render_images(self, tokens, idx, options, env):
    token = tokens[idx]
    return '<div class = "slide clear"><img src="{img}" alt="" width="426" /></div>'.format(img=token.attrs["src"])

def run():

    #Input
    root = Path.cwd()
    create_or_reset_if_exist(root / "screenshots")

    run_decktape(root / "slide")
    dict_img = get_img_slides(root / "screenshots" )
    dict_comments = get_md_comments(root / "comments")
    print(list(Path(root / "comments").glob('**/*')))

    # Output
    out = Path(root / "output")
    resources = out.joinpath("resources")
    screen = out.joinpath("screenshots")

    create_or_reset_if_exist(out)
    #create_or_reset_if_exist(resources)
    #create_or_reset_if_exist(screen)

    mdFile = MdUtils(file_name='slide', title='Markdown File Example')
    mdFile.new_header(level=1, title='Présentation du 3/12/2021')
    result = generate_globalMdFile(mdFile, root, dict_img, dict_comments)
    result.create_md_file()

    md = (
        MarkdownIt()
        .use(front_matter_plugin)
        .use(footnote_plugin)
        .enable('image')
        .enable('table')
    )

    md.add_render_rule("image", render_images)

    content = mdFile.read_md_file("slide")
    tokens = md.parse(content)
    html_text = md.render(content)
    html = """
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <base target="_top">
    
        <title>Slide 3/12/2021</title>
        <link href='http://fonts.googleapis.com/css?family=Asap:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="resources/main.css" type="text/css">
    </head>
    <body>
        {body}
    </body></html>
    """.format(body=html_text)

    print("EXPORT TO HTML")



    out.joinpath("index.html").write_text(html)


    shutil.copytree ( src= root / "resources",  dst=resources )


    shutil.copytree ( src=root / "screenshots", dst=screen )

