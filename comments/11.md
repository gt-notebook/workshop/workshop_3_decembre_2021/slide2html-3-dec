Cet atelier est séparé en deux parties, théorique et pratiques.
Dans la partie théorique, qui ne dépassera pas une heure, nous allons parler de l'historique et de la nature même de l'objet. Cela permettra de faire le lien avec la question de la reproductibilité, pregnante depuis quelque temps dans l'actualité scientifique.

Nous questionnerons aussi les pratiques, les termes autour d'un objet technique qui questionne nos modes de construction de connaissances en général, et plus particulièrement en inter-disciplinaire. Ce qui nous amènera à un débat intéressant, celui de la place de cet objet dans le cadre des questionnements actuels autour du cycle de vie des données de la recherche.

Enfin, nous essaierons de donner quelques pistes/pointeurs vers des initiatives existantes en France et en Europe.

Une partie pratique, plus longue, permettra de prendre mesure des possibilités offertes, dans différentes disciplines (Biologie/Epidémiologie et SHS), par cet objet.
