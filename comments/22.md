Autre fonctionnalité, les Chunk peuvent également être paramétré au moment de leur execution.

Dans cet exemple, le Chunk somme prend un paramètre a et un paramètre b, les deux sont initialisés avec des valeurs par défaut à 0.

Le bloc _calcul_complexe_ fait appel au bloc _somme_ en passant des valeurs pour a = 3 et b = 5. 

De la même façon que les fonctions en informatique ou en mathématique, la possibilité de paramétrer les Chunk permet une grande modularité dans la composition des documents.