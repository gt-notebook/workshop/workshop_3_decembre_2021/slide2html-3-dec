Ce concept de chunk a été repris et opérationnalisé dans le cadre de différents outils, de différents languages.

a) Knuth a assez logiquement écrit les premiers outils pour le LP, aussi afin de démontrer la faisabilité de sa démarche : WEB (Pascal), puis CWEB (C). NoWeb est une version plus récente de Web, elle est multilangages et surtout plus simple.
b) Org-mode et org-babel permettent de faire du LP via le logiciel Emacs
c) Jupyter, RMarkdown sont les outsiders les plus récents. Si RMarkdown reprend la plupart des principes du LP, ce n'est le cas des chunk proposées par Jupyter.
